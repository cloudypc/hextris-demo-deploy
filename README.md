# hextris-demo
A very simple (hopefully fun) example of application developement and subsequent deployment.

# Building and running the application locally
The application comes with a pre-configured Dockerfile which enables building the application locally.

To build a docker image:
```
git clone https://gitlab.its.deakin.edu.au/connellp/hextris-demo.git
cd hextris
docker build . -t hextris-demo
```

To run the docker image:
```
docker run -p 8000:80 hextris-demo
```

# Credit
The application within the app folder was developed via https://github.com/Hextris/hextris. Full credit goes to the maintainers of that repository.